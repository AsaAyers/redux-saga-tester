# @AsaAyers/redux-saga-tester

## Project Status: Experimental

Expect every 0.x release to break the API. Nothing is settled.

I am releasing this under my namespace because [3LOK][3LOK] already [has a
`redux-saga-tester`][redux-saga-tester]. When the API settles and I want to
release a 1.0.0, I'd like to have a [different
name](https://gitlab.com/AsaAyers/redux-saga-tester/issues/1).

## Project Goals

* Tests should not be tightly coupled to implementation
* Tests should read in the same order as the saga
  * I think this makes them easier to read and understand

## Example

For this example I created a toy saga based on [3LOK's comment][comment].

```js
function* addSaga() {
    const a = yield select(selectA)
    const b = yield select(selectB)

    const total = yield call(sum, a, b)

    yield put({
        type: "RESULT",
        payload: total
    })
}
```

I want to verify that this saga uses `sum()` to add `store.a` and `store.b` and
updates `store.result` with the total. By default the tester doesn't tell you
about `yield select` or `yield put`. they happen automatically.

```js
import { addSaga, sum, reducer, selectResult, selectA, selectB } from "./add-saga.js"
test("add-saga", () => {
    const tester = SagaTester({
        initialState: {
            a: 2,
            b: 3,
        },
        reducer,
    })

    tester.testAgainst(addSaga, function* ({ getState }) {
        let actual

        // yield gives you the next effect from your saga that you want to test.
        // This didn't care about the selectors, they execute normally.
        actual = yield
        expect(actual).toEqual(call(sum, 2, 3))
        const callSumResult = 5

        // You need to yield a result back to the saga to continue and pick up
        // the next significant effect.
        actual = yield callSumResult
        // END is a special value automatically emitted when/if your saga ends
        expect(actual).toBe(END)

        // Instead of verifying the action was fired, now verify the state.
        actual = selectResult(getState())
        expect(actual).toBe(callSumResult)
    })
})
```

This test and some other variations are available in
[`src/tests/add-saga.test.js`](./src/tests/add-saga.test.js).

[redux-saga-tester]: https://www.npmjs.com/package/redux-saga-tester
[3LOK]: https://github.com/3LOK
[redux-saga]: http://yelouafi.github.io/redux-saga/
[comment]: https://github.com/yelouafi/redux-saga/issues/518#issuecomment-246527347
