import createSagaMiddleware, { utils } from "redux-saga"
import { put, take, fork } from "redux-saga/effects"
import { createStore, applyMiddleware } from "redux"

const INTERCEPT = "@@saga-tester/intercept"
const RESPONSE = "@@saga-tester/response"
export const END = { end: true }
export const EXECUTE_EFFECT = { executeEffect: true }

const defaultCapture = {
    ...Object.keys(utils.asEffect).reduce(
        (memo, key) => ({...memo, [key]: true} )
    ),
    // By default, don't capture select or put, they are better tested using
    // `initialState` and selectors.
    select: false,
    put: false,
}

const getEffectType = (effect) => Object.keys(utils.asEffect).find(
    key => utils.asEffect[key](effect)
)

function* sagaWrap(userSaga, captureEffects) {
    const generator = userSaga()

    let next = generator.next()

    while (!next.done) {
        let response = undefined
        const type = getEffectType(next.value)

        let shouldCapture = captureEffects[type]
        if (typeof captureEffects[type] === "function") {
            shouldCapture = captureEffects[type](next.value)
        }

        if (shouldCapture) {
            yield put({
                type: INTERCEPT,
                payload: next.value
            })

            const { payload } = yield take(RESPONSE)
            if (payload === EXECUTE_EFFECT) {
                response = yield next.value
            } else {
                response = payload
            }

        } else {
            response = yield next.value
        }

        next = generator.next(response)
    }

    yield put({
        type: INTERCEPT,
        payload: END
    })
}

function* testSaga(testGenerator, store) {
    const generator = testGenerator({
        getState: store.getState
    })

    let next = generator.next()

    while (!next.done) {
        const tmp = yield take(INTERCEPT)

        const next = generator.next(tmp.payload)

        yield put({
            type: RESPONSE,
            payload: next.value
        })
    }
}

export default function SagaTester(options) {
    const {
        initialState = {},
        reducer = state => state,
        middleware = [],
        captureEffects = defaultCapture
    } = options

    const sagaMiddleware = createSagaMiddleware()

    const allMiddlewares = [
        ...middleware,
        sagaMiddleware,
    ]

    const store = createStore(
        reducer,
        initialState,
        applyMiddleware(...allMiddlewares)
    )


    return {
        testAgainst(saga, generatorFn) {
            sagaMiddleware.run(function* () {
                yield fork(testSaga, generatorFn, store)
                yield fork(sagaWrap, saga, captureEffects)
            })

        }
    }
}
