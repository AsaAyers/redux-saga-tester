import { utils } from "redux-saga"
import { call, select, put } from "redux-saga/effects"
import { addSaga, sum, reducer, selectResult, selectA, selectB } from "./add-saga.js"

import SagaTester, { END, EXECUTE_EFFECT } from "../index.js"

// You can try to test sagas this way, but it's fragile, and I don't think it
// asserts that your saga works in your app. The whole saga has been duplicated
// here and reformatted to look like a test.
test("Raw Generator", () => {
    let next = undefined
    const generator = addSaga()

    // It doesn't matter what order these selects happen in, but if you flip
    // them your test will break.
    next = generator.next()
    expect(next.value).toEqual(select(selectA))

    next = generator.next(2)
    expect(next.value).toEqual(select(selectB))

    next = generator.next(3)
    expect(next.value).toEqual(call(sum, 2, 3))

    // Do you really care that the action is fired, or do you need to know that
    // the store is updated?
    next = generator.next(5)
    expect(next.value).toEqual(put({
        type: "RESULT",
        // Are you sure this is the format your reducer expects? Does your
        // reducer expect `payload: { total: 5 }`? or do you have a typo and
        // your reducer expects `paylod: 5`
        payload: 5
    }))
})

test("add-saga", () => {
    const tester = SagaTester({
        initialState: {
            a: 2,
            b: 3,
        },
        reducer,
    })

    tester.testAgainst(addSaga, function* ({ getState }) {
        let actual

        // yield gives you the next effect from your saga that you want to test.
        // This didn't care about the selectors, they execute normally.
        actual = yield
        expect(actual).toEqual(call(sum, 2, 3))
        const callSumResult = 5

        // You need to yield a result back to the saga to continue and pick up
        // the next significant effect.
        actual = yield callSumResult
        // END is a special value automatically emitted when/if your saga ends
        expect(actual).toBe(END)

        // Instead of verifying the action was fired, now verify the state.
        actual = selectResult(getState())
        expect(actual).toBe(callSumResult)
    })
})


test("add-saga with custom filters", () => {
    const tester = SagaTester({
        initialState: {
            a: 2,
            b: 3,
        },
        reducer,
        captureEffects: {
            call: true,
            // Capturing selected actions might be useful if they get picked up
            // by other sagas instead of changing the store.
            put: (effect) => {
                const e = utils.asEffect.put(effect)
                return e.action.type === "RESULT"
            }
        }
    })

    tester.testAgainst(addSaga, function* ({ getState }) {
        let actual

        actual = yield
        expect(actual).toEqual(call(sum, 2, 3))

        actual = yield 5
        expect(actual).toEqual(put({
            type: "RESULT",
            payload: 5
        }))


        actual = selectResult(getState())
        // That action wasn't executed because it was captured
        expect(actual).toBe(undefined)

        // You may want to execute it normally (this isn't limited to actions,
        // this can be used for anything you intercepted.)
        actual = yield EXECUTE_EFFECT
        expect(actual).toBe(END)

        actual = selectResult(getState())
        // Now we can see the store updated because that action was executed
        expect(actual).toBe(5)
    })
})
