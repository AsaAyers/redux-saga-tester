import { select, put, call } from "redux-saga/effects"

export const reducer = (state = {}, action) => {
    if (action.type === "RESULT") {
        return {
            ...state,
            result: action.payload
        }
    }
    return state
}

export const sum = (a, b) => a + b

export const selectA = (state) => state.a
export const selectB = (state) => state.b
export const selectResult = (state) => state.result

export function* addSaga() {
    const a = yield select(selectA)
    const b = yield select(selectB)

    const total = yield call(sum, a, b)

    yield put({
        type: "RESULT",
        payload: total
    })
}
